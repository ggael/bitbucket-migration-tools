
# This script iterates over all fork of the eigen/eigen repository.
# Each fork is cloned within the forks/ subfolder and a report
# in mardown format is printed to the standard output.
#
# This script is typically ran as:
#
# $ python -u  archive_forks.py | tee archive_forks_log.md

import json
import pybitbucket
import subprocess
from subprocess import Popen, call
from pybitbucket.bitbucket import Bitbucket
import sys
import re
import os.path

bb = Bitbucket()

url = 'https://api.bitbucket.org/2.0/repositories/{}/{}/forks'.format('eigen','eigen')

def make_md_code(text):
    return "    " + text.replace("\n", "\n    ")
  
while True:
    resp = bb.client.session.get(url)

    result = resp.json()
    # print(result)
    for fork in result['values']:
        clone_url = fork['links']['clone'][0]['href']
        clone_name = fork['full_name']
        clone_cmd = "hg clone " + clone_url + " forks/" + clone_name
        print('# ' + clone_name)
        print(fork['description'] + '  \n')
        print('**url**: ' + clone_url + '  ')
        if fork['mainbranch']:
            print('**mainbranch**: ' + str(fork['mainbranch']) + '  ')
        print('**uuid**: ' + fork['uuid'] + '  ')
        owner_str = fork['owner']['display_name'];
        if 'nickname' in fork['owner']:
            owner_str += ' ; ' + fork['owner']['nickname'];
        if 'uuid' in fork['owner']:
            owner_str += ' ; ' + fork['owner']['uuid'];
        print(('**owner**: ' + owner_str + '  ').encode('utf-8'))
        
        clone_ok = True
        if os.path.exists("forks/" + clone_name):
            call("cd forks/" + clone_name + " && hg pull -u ; cd -", shell=True)
        else:
            cmd = Popen(clone_cmd,
                        shell=True,
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.STDOUT)
            stdout,stderr = cmd.communicate()
            clone_ok = cmd.returncode==0
          
        if not clone_ok:
          print('**INVALID**')
        else:
            # print("\n" + make_md_code(stdout))
            get_changes_cmd = "cd forks/" + clone_name + " && hg out https://bitbucket.org/eigen/eigen ; cd - > /dev/null"
            cmd = Popen(get_changes_cmd,
                        shell=True,
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.STDOUT)
            stdout,stderr = cmd.communicate()
            if cmd.returncode==0:
                print("\n" + make_md_code(stdout))
            else:
                print("\n    no change")
        print('\n')

    try:
        url = result['next']
    except Exception as e:
        break
