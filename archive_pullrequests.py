
# This script iterates over all pull-requests of the repo_user/repo_name bitbucket repository.
# Each pull-request of id XX is archived within the prefix/XX subfolder as follows:
#  - the summary and comments as both a static index.html file and prXX.md file
#  - the patch and diff files
#  - the raw pull request data in raw.json
#  - the raw comment pages in comments_p*.json*
#
# This script also archives raw PR pages in prefix/raw_data/page_*.json,
# and summary files:
#  - prefix/summary.txt     (1 line per pull-request)
#  - prefix/index.html      (same but as an html table with links)
#  - prefix/OPEN.html       (same as index.hmtl but for open PR only)
#  - prefix/DECLINED.html   (you got the idea!)
#  - prefix/MERGED.html
#
# The CSS file "eigen_pullrequests.css" needs to be copied by hand
# within the prefix/ folder.
#
# This script is typically ran as:
#
# $ python -u  archive_pullrequests.py


# TODO:
# - translate mercurial hashes to git hashes
# - translate links to bugzilla issues
# - translate links to bitbucket

import json
import pybitbucket
import subprocess
from subprocess import Popen, call
from pybitbucket.bitbucket import Bitbucket
import sys
import re
import os
import wget
import anytree
from anytree import Node

#---------------
# configuration
#---------------

conf = {
  'repo_user':'eigen',
  'repo_name':'eigen',
  'markdown':True,
  'html':True,
  'prefix':'pullrequests/'
}


#---------------
# templates
#---------------

MD_HEADER_TPL = """# PR {prid}: {title}  \n\
{description}  \n\n\
**author**: {author}  \n\
**source**: {src_name} / {src_branch} / {src_commit}  \n\
**destination:** {dst_branch} / {dst_commit}
**{state}**  \n\
\n\
# Comments ({nb_comments})  \n\n\
"""

MD_COMMENT_TPL = """<div style="margin-left:{indent}em">  \n\n\
### {author}  \n\n\
<div style="border-left:1px solid;border-bottom:1px solid;padding-left:0.5em;padding-bottom:0.5em">  \n\n\
{body}  \n\n\
</div></div>  \n\
"""

HTML_HEADER = """<!doctype html>\n\
<html lang="en">\n\
<head>\n\
  <meta charset="utf-8">\n\
  <title>{title}</title>\n\
   <link rel="stylesheet" href="../eigen_pullrequests.css">\n\
</head>\n\
<body>\n\
"""

HTML_PR_HEADER = """<!doctype html>\n\
<html lang="en">\n\
<head>\n\
  <meta charset="utf-8">\n\
  <title>PR {prid}: {title}</title>\n\
   <link rel="stylesheet" href="../eigen_pullrequests.css">\n\
</head>\n\
<body>\n\
<h1>PR {prid}: {title} <span id="pr_state" class="{state}">{state}</span></h1>\n\
<p class="pr_info">\n\
<strong>author</strong>: {author}<br/>\n\
<strong>source</strong>: {src_name} / {src_branch} / {src_commit}<br/>\n\
<strong>destination</strong>: {dst_branch} / {dst_commit}<br/>\n\
<strong>links</strong>:
  <a href="diff">diff</a> 
  <a href="patch">patch</a><br/>\n\
<strong>raw data</strong>: 
  <a href="raw.json">raw.json</a>,
  comment pages: {comment_pages},
  <a href="bb_dump.html">bb_dump.html</a><br/>\n\
</p>\n\
<p class="pr_description">\n\
{description}\n\
</p>\n\
<h2 id="comment_header">Comments ({nb_comments})</h2>\n\
"""

HTML_COMMENT = """
<div class="comment" style="margin-left:{indent}em">\n\
<a id="{comment_id}"></a>
<h3>{author} <a class="goto_parent" href="#{parent_id}"></a></h3>\n\
<div class="comment-content">\n\
{body}\n\
</div></div>\n\
"""

HTML_SUMMARY_LINE = """
<tr>
<td><a href="{prid}/">{prid}</a></td>
<td><a href="{state}.html">{state}</a></td>
<td><a href="{prid}/">{title}</a></td>
<td>{author}</td>
<td>{src_name}</td>
</tr>
"""

HTML_FOOTER = """</body>\n\
</html>\n\
"""

#---------------


bb = Bitbucket()

url = 'https://api.bitbucket.org/2.0/repositories/{}/{}/pullrequests?state=all'.format(conf['repo_user'],conf['repo_name'])

def make_md_code(text):
    return "    " + text.replace("\n", "\n    ")


def wget_overwrite(url,dest):
    if os.path.exists(dest):
        os.remove(dest)
    wget.download(url,dest)
    print("")

summary_file = open(conf['prefix']+"summary.txt", "w")

html_files = {}
if conf['html']:
    html_files['summary'] = open(conf['prefix']+"index.html", "w")
    html_files['summary'].write(HTML_HEADER.format(title="{}/{} - pull-request summary".format(conf['repo_user'],conf['repo_name'])))
    html_files['summary'].write("<table><tr><th>id</th><th>state</th><th>title</th><th>author</th><th>source</th></tr>")

pr_count_page = 1

if not os.path.exists(conf['prefix']+"raw_data"):
    os.makedirs(conf['prefix']+"raw_data")

while True:
    wget_overwrite(url, conf['prefix']+"raw_data/page_" + str(pr_count_page) + ".json")
    resp = bb.client.session.get(url)

    result = resp.json()
    # print(result)
    for pr in result['values']:
      
        pr_id    = pr['id']
        pr_state = pr['state']
      
        pr_folder = conf['prefix']+"{id}".format(id=pr_id,state=pr_state)
        
        if not os.path.exists(pr_folder):
          os.makedirs(pr_folder)
          
        print(pr['id'])
        
        src_repo = 'null'
        if pr['source']['repository']:
          src_repo = pr['source']['repository']['full_name']
          
        src_commit = 'null'
        if pr['source']['commit']:
          src_commit = pr['source']['commit']['hash']
        
        dst_commit = 'null'
        if pr['destination']['commit']:
          dst_commit = pr['destination']['commit']['hash']
          
        author = 'null'
        if pr['author']:
          author = pr['author']['display_name'].encode('utf-8')
          
        d = {'prid':        str(pr['id']),
             'title':       pr['title'].encode('utf-8'),
             'description': pr['description'].encode('utf-8'),
             'author':      author,
             'src_name':    src_repo,
             'src_branch':  pr['source']['branch']['name'],
             'src_commit':  src_commit,
             'dst_branch':  pr['destination']['branch']['name'],
             'dst_commit':  dst_commit,
             'nb_comments': pr['comment_count'],
             'state':       pr['state']}
          
        summary_file.write("{prid} ; {state} ; {title} ; {author} ; {src_name}\n".format(**d))
        
        pr_header = MD_HEADER_TPL.format(**d)
        
        print(pr_header)
        
        print("save self json file...")
        # compared to teh summary, it includes the list of all participants
        wget_overwrite(pr['links']['self']['href'], pr_folder + "/raw.json")
        
        print("save diff file...")
        wget_overwrite(pr['links']['diff']['href'], pr_folder + '/diff')
        
        print("save patch file...")
        wget_overwrite(pr['links']['self']['href']+"/patch", pr_folder + '/patch')
        
        print("save html file...")
        wget_overwrite(pr['links']['html']['href'], pr_folder + '/bb_dump.html')
        
        if conf['markdown']:
          pr_md_file = open(pr_folder + "/pr" + str(pr['id']) + ".md", "w")
          pr_md_file.write(pr_header + "\n\n")

        
        # save comments in a tree
        root = Node('root')
        count_comment_page = 1
        comment_url = pr['links']['comments']['href'] + '?pagelen=100'
        comment_page_links = ""
        print("download comments...")
        while True:
            wget_overwrite(comment_url, pr_folder + "/comments_p" +  str(count_comment_page) + ".json")
            resp_comments = bb.client.session.get(comment_url)
            comments = resp_comments.json()
            #print(comments)
            for comment in comments['values']:
                parent = root
                if 'parent' in comment:
                    # search parent from id
                    parent = anytree.find(root, lambda node: node.name == comment['parent']['id'])
                n = Node(comment['id'],parent,content=comment)
            comment_page_links += '<a href="comments_p{page}.json">{page}</a>  '.format(prid=pr['id'],page=count_comment_page)
            
            count_comment_page += 1
            if 'next' in comments:
                comment_url = comments['next']
            else:
                break;
              
        if pr['summary']:
            d['description'] = pr['summary']['html'].encode('utf-8')
        d['comment_pages'] = comment_page_links 
        
        if conf['html']:
            if not pr_state in html_files:
                html_files[pr_state] = open(conf['prefix']+pr_state+".html", "w")
                html_files[pr_state].write(HTML_HEADER.format(title="Pull-requests - {}".format(pr_state)))
                html_files[pr_state].write("<table><tr><th>id</th><th>state</th><th>title</th><th>author</th><th>source</th></tr>")
            html_files['summary'].write(HTML_SUMMARY_LINE.format(**d))
            html_files[pr_state].write(HTML_SUMMARY_LINE.format(**d))
            pr_html_file = open(pr_folder + "/index.html", "w")
            pr_html_file.write(HTML_PR_HEADER.format(**d))
        
        # render the comments
        for node in anytree.PreOrderIter(root):
          if node.name != 'root':
            comment = node.content
            author_display_name = 'null'
            if comment['user']:
                author_display_name = comment['user']['display_name'].encode('utf-8')
            comment_parent_id = comment['id']
            if 'parent' in comment:
                comment_parent_id = comment['parent']['id']
            
            if conf['markdown']:
                pr_md_file.write(MD_COMMENT_TPL.format(author =  author_display_name,
                                                       indent =  (node.depth-1)*2,
                                                       body   = (comment['content']['raw']).encode('utf-8')) + '\n\n')
                            
            if conf['html']:
                pr_html_file.write(HTML_COMMENT.format(author =  author_display_name,
                                                       indent =  (node.depth-1)*2,
                                                       body = (comment['content']['html']).encode('utf-8'),
                                                       comment_id = comment['id'],
                                                       parent_id = comment_parent_id))
              
        if conf['markdown']:
            pr_md_file.close()
        
        if conf['html']:
            pr_html_file.write(HTML_FOOTER)
            pr_html_file.close();
        
        
        print('\n')

    if 'next' in result:
        pr_count_page += 1
        url = result['next']
    else:
        break

summary_file.close()

if conf['html']:
    for f in html_files:
        f.write("</table>")
        f.write(HTML_FOOTER)
        f.close()
